-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 30, 2018 at 07:49 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.0.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `node_awesomestore`
--

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `name`, `logo`, `created_at`, `updated_at`) VALUES
(1, 'Timberland', '2296d68fff1562ec52d9972cafce2d54', '2018-07-15 16:50:01', '2018-07-22 10:19:35'),
(2, 'Nobody Jeans', '4161a9257a740faf53695b6faf6710d2', '2018-07-15 16:53:16', '2018-07-22 10:19:45'),
(3, 'ERKE', '88d5baf0aa4543e9538db4c170899ef4', '2018-07-15 16:53:27', '2018-07-22 16:03:20'),
(4, 'H&M', '67ef6d60ed6f6980ddb6c727f193ae75', '2018-07-15 16:53:47', '2018-07-22 16:04:50'),
(5, 'Forever 21', 'f348463f4f0a8a4ed42ec048284d402e', '2018-07-15 16:54:00', '2018-07-22 16:05:26');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `icon`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, 'MEN\'S FASHION', NULL, NULL, '2018-07-15 15:52:02', NULL),
(2, 'WOMEN\'S FASHION', NULL, NULL, '2018-07-15 15:52:02', NULL),
(3, 'BEAUTY AND HEALTH', NULL, NULL, '2018-07-15 15:52:39', NULL),
(4, 'PHONES AND TABLETS', NULL, NULL, '2018-07-15 15:52:39', NULL),
(5, 'COMPUTING AND GAMING', NULL, NULL, '2018-07-15 15:53:21', NULL),
(6, 'TVS, AUDIO AND CAMERAS', NULL, NULL, '2018-07-15 15:53:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` int(11) NOT NULL,
  `name` varchar(155) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `name`, `type`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, 'Yangon', 1, 0, '2018-07-15 10:23:38', '0000-00-00 00:00:00'),
(2, 'Mandalay', 1, 0, '2018-07-15 10:23:38', '2018-07-15 14:56:34'),
(3, 'Magway', 1, 0, '2018-07-15 10:40:28', '2018-07-15 14:56:21'),
(4, 'Bago', 1, 0, '2018-07-17 16:49:20', '2018-07-17 16:50:25'),
(5, 'Shan', 1, 0, '2018-07-17 17:19:06', '2018-07-17 17:47:46'),
(6, 'Kachin', 1, 0, '2018-07-17 17:47:17', '2018-07-17 17:48:05'),
(7, 'Mon', 1, 0, '2018-07-17 17:49:17', '2018-07-17 17:49:29'),
(8, 'Bagan', 1, 0, '2018-07-28 09:00:40', '2018-07-28 20:26:23'),
(9, 'Nay Pyi Taw', 1, 0, '2018-07-28 09:00:51', '2018-07-28 20:26:36'),
(10, 'Kalaw', 2, 0, '2018-07-28 09:04:16', '2018-07-28 20:26:40');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_by` int(11) NOT NULL,
  `types` int(11) NOT NULL COMMENT '0=>article, 1=>event',
  `township_id` int(11) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `status` int(10) UNSIGNED DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `thumbnail`, `description`, `created_by`, `types`, `township_id`, `start_date`, `end_date`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Testing', '180e759bb62e717f8a89ca153e57d190', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellat explicabo ut veritatis dignissimos dolores consectetur totam pariatur reprehenderit architecto asperiores eum harum, quia labore, cumque perferendis, vel eos? Eum, sequi.', 0, 0, 1, '2018-07-29 00:00:00', '2018-07-31 00:00:00', 1, '2018-07-28 21:37:07', '2018-07-28 21:37:07');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `price` float NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `thumbnail`, `category_id`, `brand_id`, `price`, `description`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'Bradstreet Oxford Shoe - Grey', '8882d5836f82ebf555be458852122104', 1, 1, 199000, 'Key Features\r\nCasual Wear\r\nFree style\r\nCountry of origin - America\r\nBest Quality', 0, '2018-07-22 15:44:50', '2018-07-22 15:44:50'),
(2, 'Bradstreet Oxford Shoe - Brown', '4783e7997bfe51a2fdf74ac83e94f3ce', 1, 1, 199000, 'Key Features\r\nCasual Wear\r\nFree style\r\nCountry of origin - America\r\nBest Quality', 0, '2018-07-22 15:47:48', '2018-07-22 15:47:48'),
(3, 'Floral Printed with Black Base', 'b7916b74da903f9f16014ed07d7bef06', 2, 4, 41800, 'Its creation of combining with neat, accurate and unique beauty designs will very compatible with every lady.', 0, '2018-07-22 15:52:47', '2018-07-22 15:52:47'),
(4, 'Light Blue & Pink Printed with White Base', 'cbe9e693bd64a2d97c459ee37ccf0913', 2, 4, 30400, 'Its creation of combining with neat, accurate and unique beauty designs will very compatible with every lady. ', 0, '2018-07-22 15:58:25', '2018-07-22 15:58:25');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `session_id` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `expires` int(11) UNSIGNED NOT NULL,
  `data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`session_id`, `expires`, `data`) VALUES
('3SkCS2m5wMFzB-B7cS3uy6FKTo1HU1bC', 1532942472, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"},\"flash\":{}}'),
('DEAf-DtoH1BK0BTDjOpLmLRhWZRlINfr', 1532969833, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"},\"flash\":{}}'),
('LjVhA0Q2MIQDXTKD90RurL6yCYNR4g5f', 1532972662, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"},\"flash\":{}}'),
('NhYsoOJXGNgMP08fr5INVrb6jsmfy-8E', 1532941591, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"},\"flash\":{}}'),
('Q0fHOqRstqPR83-fIxKSTavxMsuRPMS8', 1532942145, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"},\"flash\":{}}'),
('Qs2DaDNPnZKiy0JEdjX9ZeREjzdm36Uu', 1532973953, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"},\"flash\":{},\"returnTo\":\"/\",\"passport\":{\"user\":1}}'),
('WamGqYEB5034Eqb6hH54-nricyhX783M', 1532946215, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"},\"flash\":{}}'),
('gk9-k2-4NZc5aQ_33uUvhtCTYacLlNLW', 1532943679, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"},\"flash\":{}}'),
('iyTwlUaOw3j5jhfPVsvIf7PEyCKYihpN', 1532944616, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"},\"flash\":{}}');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` int(11) NOT NULL DEFAULT '1' COMMENT '1=>Customer',
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `phone`, `role`, `address`, `photo`, `created_at`, `updated_at`) VALUES
(1, 'Khin Zaw Lwin', 'khinzawlwin.mm@gmail.com', '$2a$10$pPqcQ8DPY0hpqxtDvcumEe1TtY/QPAXTamQLI8WdyfX.PEaQnfN.2', '0912345678', 3, 'Hlaing, Yangon.', NULL, '2018-07-15 17:26:52', '2018-07-29 09:06:37'),
(2, 'May Thu', 'maythu@gmail.com', '$2a$10$pPqcQ8DPY0hpqxtDvcumEe1TtY/QPAXTamQLI8WdyfX.PEaQnfN.2', '0912345678', 3, 'Hlaing Tsp, Yangon', NULL, '2018-07-15 17:28:52', '2018-07-29 09:06:41'),
(3, 'Moe Win', 'moewin@gmail.com', '$2a$10$VkveOGZnfLjPpGgsJhZvf.0UwXCTRT5B5ZvI68/AhF1MmGNZIs/h2', '0912345678', 0, 'No.19, Tamwe, Yangon', NULL, '2018-07-22 10:10:07', '2018-07-29 09:06:55'),
(4, 'Kyaw Zin', 'kyawzin@gmail.com', '$2a$10$VkveOGZnfLjPpGgsJhZvf.0UwXCTRT5B5ZvI68/AhF1MmGNZIs/h2', '0912345678', 1, 'No.19, Hlaing Tharyar, Yangon', NULL, '2018-07-28 19:58:31', '2018-07-29 09:07:01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`session_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`,`phone`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
