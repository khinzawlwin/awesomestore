var express = require('express');
var router = express.Router();
var models = require('../models');
var Sequelize = require('sequelize');
var Op = Sequelize.Op;
var multer  = require('multer')
var thumbnailUpload = multer({ dest: 'public/uploads/' });
var { check, validationResult } = require('express-validator/check');
var auth = require('connect-ensure-login').ensureLoggedIn;

/* GET brands. */ //async... await
router.get('/', auth('/auth/login') ,async function(req, res, next) {

  let products = await models.Product.findAll();
  res.render('products/list', {products: products});
});

/* GET create brand form. */
router.get('/create', auth('/auth/login'), async function(req, res, next) {
  let categories = await models.Category.findAll();
  let brands = await models.Brand.findAll();
    res.render('products/create', {categories: categories, brands: brands});
});

/* POST create brand form. */
router.post('/store', thumbnailUpload.single('thumbnail'), async function(req, res, next) {
  let formData = req.body;
  let photo = req.file;
  if(photo){
    formData.thumbnail = photo.filename;
  }
  let result = await models.Product.create(formData);

  // const errors = validationResult(req);
  // if(!errors.isEmpty){
  //   req.flash('errors', errors.array());
  //   return res.redirect('/products/create');
  // }

  if(result){
    req.flash('infos', 'Successfylly submit your brand!');
    return res.redirect('/products');
  }
});

/* GET edit brand form. */
router.get('/:id/edit', auth('/auth/login'), async function(req, res, next) {
  let id = req.params.id;
  let categories = await models.Category.findAll();
  let brands = await models.Brand.findAll();

  let brand = await models.Product.findById(id);
  res.render('products/edit', {brand: brand, categories: categories, brands: brands});
});

/* POST update brand form. */
router.post('/:id/update', thumbnailUpload.single('thumbnail'),async function(req, res, next){
  let id = req.params.id;
  let formData = req.body;
  let photo = req.file;
  if(photo){
    formData.thumbnail = photo.filename;
  }
  let result = await models.Product.update(formData, {where: {id:id}});

  if(result){
    req.flash('infos', 'Successfylly update your brand!');
    res.redirect('/products');
  }
});

/* GET delete brand form. */
router.get('/:id/delete', auth('/auth/login'), async function(req, res, next){
  let id = req.params.id;
  let result = await models.Product.destroy({where:{id:id}});

  if(result){
    req.flash('infos', 'Successfylly delete your brand!');
    res.redirect('/products');
  }
});

module.exports = router;