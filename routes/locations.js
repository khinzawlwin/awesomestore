var express = require('express');
var router = express.Router();
var models = require('../models');
var Sequelize = require('sequelize');
var Op = Sequelize.Op;
var { check, validationResult } = require('express-validator/check');
var auth = require('connect-ensure-login').ensureLoggedIn;

/* GET locations. */ //async... await
router.get('/', auth('/auth/login'), async function(req, res, next) {
  let locations = await models.Location.findAll();
  res.render('locations/list', {locations: locations});
});

/* GET locations. */ //promise
// router.get('/', function(req, res, next) {
//   models.Location.findAll().then((result)=>{
//     res.render('locations/list', {locations: locations});
//   });
// });


/* GET create location form. */
router.get('/create', auth('/auth/login'), function(req, res, next) {

    res.render('locations/create');
});

/* POST create location form. */
router.post('/store', [
  check('name', 'Name must be at least 3 character!').isLength({min:3})
],async function(req, res, next) {
  let formData = req.body;
  const errors = validationResult(req);
  if(!errors.isEmpty){
    req.flash('errors', errors.array());
    return res.redirect('/locations/create');
  }

  let result = await models.Location.create(formData);
  if(result){
    req.flash('infos', 'Successfully submit your locations!');
    return res.redirect('/locations/create');
  }
});

/* GET edit location form. */
router.get('/:id/edit', auth('/auth/login'), async function(req, res, next) {
  let id = req.params.id;
  let location = await models.Location.findById(id);
  res.render('locations/edit', {location: location});
});

/* POST update location form. */
router.post('/:id/update', async function(req, res, next){
  let id = req.params.id;
  let formData = req.body;
  let result = await models.Location.update(formData, {where: {id:id}});

  if(result){
    req.flash('infos', 'Successfully update your locations!');
    res.redirect('/locations');
  }
});

/* GET delete location form. */
router.get('/:id/delete', auth('/auth/login'), async function(req, res, next){
  let id = req.params.id;
  let result = await models.Location.destroy({where:{id:id}});

  if(result){
    res.flash('infos', 'Successfully delete your locations!');
    res.redirect('/locations');
  }
});

module.exports = router;