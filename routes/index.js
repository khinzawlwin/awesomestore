var express = require('express');
var router = express.Router();
var auth = require('connect-ensure-login').ensureLoggedIn;

/* GET home page. */
router.get('/', auth('/auth/login'), function(req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;
