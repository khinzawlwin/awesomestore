var express = require('express');
var router = express.Router();
var models = require('../models');
var Sequelize = require('sequelize');
var Op = Sequelize.Op;
var multer = require('multer');
var thumbnailUpload = multer({ dest: 'public/uploads/' });
var { check, validationResult } = require('express-validator/check');
var auth = require('connect-ensure-login').ensureLoggedIn;

/* GET categories. */ //async... await
router.get('/', auth('/auth/login'), async function(req, res, next) {
  let categories = await models.Category.findAll();
  res.render('categories/list', {categories: categories});
});

/* GET create category form. */
router.get('/create', auth('/auth/login'), function(req, res, next) {

    res.render('categories/create');
});

/* POST create category form. */
router.post('/store', thumbnailUpload.single('icon'),async function(req, res, next) {
  let formData = req.body;
  let photo = req.file;
  if(photo){
    formData.icon = photo.filename;
  }
  let result = await models.Category.create(formData);

  const errors = validationResult(req);
  if(!errors.isEmpty){
    req.flash('errors', errors.array());
    return res.redirect('/categories/create');
  }

  if(result){
    req.flash('infos', 'Successfully submit your category!');
    return res.redirect('/categories');
  }
});

/* GET edit category form. */
router.get('/:id/edit', auth('/auth/login'), async function(req, res, next) {
  let id = req.params.id;
  let category = await models.Category.findById(id);
  res.render('categories/edit', {category: category});
});

/* POST update category form. */
router.post('/:id/update', thumbnailUpload.single('icon'),async function(req, res, next){
  let id = req.params.id;
  let formData = req.body;
  let photo = req.file;
  if(photo){
    formData.icon = photo.filename;
  }
  let result = await models.Category.update(formData, {where: {id:id}});

  if(result){
    req.flash('infos', 'Successfully update your category!');
    res.redirect('/categories');
  }
});

/* GET delete category form. */
router.get('/:id/delete', auth('/auth/login'), async function(req, res, next){
  let id = req.params.id;
  let result = await models.Category.destroy({where:{id:id}});

  if(result){
    req.flash('infos', 'Successfully delete your category!');
    res.redirect('/categories');
  }
});

module.exports = router;