var express = require('express');
var router = express.Router();
var models = require('../models');
var Sequelize = require('sequelize');
var Op = Sequelize.Op;
var { check, validationResult } = require('express-validator/check');
var bcrypt = require('bcryptjs');
var auth = require('connect-ensure-login').ensureLoggedIn;

/* GET users. */ //async... await
router.get('/', auth('/auth/login'), async function(req, res, next) {
  let users = await models.User.findAll();
  res.render('users/list', {users: users});
});

/* GET create user form. */
router.get('/create', auth('/auth/login'), function(req, res, next) {
    res.render('users/create');
});

/* POST create user form. */
router.post('/store', async function(req, res, next) {
  let formData = req.body;

  var userExists = await models.User.findAll({where:{[Op.or]: [{phone: formData.phone}, {email: formData.email}]} });
  if(userExists && userExists.length > 0){
    req.flash("errors", "Email or Phone number is already in use!");
    return res.redirect('/users/create');
  }
  var salt = bcrypt.genSaltSync(10);
  var hash = bcrypt.hashSync(formData.password, salt);
  formData.password = hash;

  let result = await models.User.create(formData);

  const errors = validationResult(req);
  if(!errors.isEmpty){
    req.flash('errors', errors.array());
    return res.redirect('/users/create');
  }

  if(result){
    req.flash('infos', 'Successfully submit user!');
    return res.redirect('/users');
  }
});

/* GET edit user form. */
router.get('/:id/edit', auth('/auth/login'), async function(req, res, next) {
  let id = req.params.id;
  let user = await models.User.findById(id);
  res.render('users/edit', {user: user});
});

/* POST update user form. */
router.post('/:id/update', async function(req, res, next){
  let id = req.params.id;
  let formData = req.body;
  var salt = bcrypt.genSaltSync(10);
  var hash = bcrypt.hashSync(formData.password, salt);
  formData.password = hash;

  let result = await models.User.update(formData, {where: {id:id}});

  if(result){
    req.flash('infos', 'Successfully update user!');
    res.redirect('/users');
  }
});

/* GET delete user form. */
router.get('/:id/delete', auth('/auth/login'), async function(req, res, next){
  let id = req.params.id;
  let result = await models.User.destroy({where:{id:id}});

  if(result){
    req.flash('infos', 'Successfully delete user!');
    res.redirect('/users');
  }
});

module.exports = router;