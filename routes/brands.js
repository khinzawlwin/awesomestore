var express = require('express');
var router = express.Router();
var models = require('../models');
var Sequelize = require('sequelize');
var Op = Sequelize.Op;
var multer  = require('multer')
var thumbnailUpload = multer({ dest: 'public/uploads/' });
var { check, validationResult } = require('express-validator/check');
var auth = require('connect-ensure-login').ensureLoggedIn;

/* GET brands. */ //async... await
router.get('/', auth('/auth/login') ,async function(req, res, next) {
  let brands = await models.Brand.findAll();
  res.render('brands/list', {brands: brands});
});

/* GET create brand form. */
router.get('/create', auth('/auth/login'), function(req, res, next) {
    res.render('brands/create');
});

/* POST create brand form. */
router.post('/store', thumbnailUpload.single('logo'),async function(req, res, next) {
  let formData = req.body;
  let photo = req.file;
  if(photo){
    formData.logo = photo.filename;
  }
  let result = await models.Brand.create(formData);

  const errors = validationResult(req);
  if(!errors.isEmpty){
    req.flash('errors', errors.array());
    return res.redirect('/brands/create');
  }

  if(result){
    req.flash('infos', 'Successfylly submit your brand!');
    return res.redirect('/brands');
  }
});

/* GET edit brand form. */
router.get('/:id/edit', auth('/auth/login'), async function(req, res, next) {
  let id = req.params.id;
  let brand = await models.Brand.findById(id);
  res.render('brands/edit', {brand: brand});
});

/* POST update brand form. */
router.post('/:id/update', thumbnailUpload.single('logo'),async function(req, res, next){
  let id = req.params.id;
  let formData = req.body;
  let photo = req.file;
  if(photo){
    formData.logo = photo.filename;
  }
  let result = await models.Brand.update(formData, {where: {id:id}});

  if(result){
    req.flash('infos', 'Successfylly update your brand!');
    res.redirect('/brands');
  }
});

/* GET delete brand form. */
router.get('/:id/delete', auth('/auth/login'), async function(req, res, next){
  let id = req.params.id;
  let result = await models.Brand.destroy({where:{id:id}});

  if(result){
    req.flash('infos', 'Successfylly delete your brand!');
    res.redirect('/brands');
  }
});

module.exports = router;